﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PROYECTO
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.ActiveControl = txtContraseña;  /*Focus caja de texto contraseña al iniciar el formulario*/
        }

        

        private void tsmSalir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Desea salir del sistema?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                Application.Exit();
            }

        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (txtContraseña.Text == "123")
            {
                error.SetError(txtContraseña, "");
                MessageBox.Show("Bienvenido al sistema de inventario", "Bienvenida", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Form2 frm2 = new Form2();
                this.Hide();
                frm2.Show();
            }
            else
            {
                error.SetError(txtContraseña, "Debe ingresar la contraseña correcta");
                txtContraseña.Focus();
                return;
            }
            error.SetError(txtContraseña, "");
        }

    }
    
}
